#!/bin/sh

PATH=/usr/bin
TMUX=/usr/bin/tmux

echo -n "checking to see that ${TMUX} exists..."

if [ -f "${TMUX}" ]; then
    echo "ok"
    RET=0
else
    echo "FAIL"
    RET=1
fi

exit ${RET}
