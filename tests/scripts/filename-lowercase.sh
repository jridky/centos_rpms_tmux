#!/bin/sh

PATH=/usr/bin
TMUX=/usr/bin/tmux

echo -n "verifying ${TMUX}'s filename contains all lowercase letters..."

BASETMUX="$(basename $(stat -L -c "%n" ${TMUX}))"
LOWERTMUX="$(echo "${BASETMUX}" | tr [A-Z] [a-z])"

if [ "${BASETMUX}" = "${LOWERTMUX}" ]; then
    echo "ok"
    RET=0
else
    echo "FAIL"
    RET=1
fi

exit ${RET}
